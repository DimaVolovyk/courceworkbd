﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkBD.DAL.Models
{
    public class Project
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(250), MinLength(1)]
        public string Name { get; set; }

        [DataType(DataType.Currency)]
        public double Cost { get; set; }
        
        [DataType(DataType.Date)]
        public TimeSpan Deadline { get; set; }

        [DataType(DataType.Date)]
        public TimeSpan WorkBegining { get; set; }

        [MinLength(1), MaxLength(500)]
        public string Address { get; set; }

        public Client Client { get; set; }

        public ICollection<Department> Departments { get; set; }
    }
}
