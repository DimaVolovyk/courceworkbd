﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkBD.DAL.Models
{
    public class Employee
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MinLength(1)]
        public string FirstName { get; set; }

        [MinLength(1)]
        public string LastName { get; set; }

        public ContactInformation ContactInformation { get; set; }

        [DataType(DataType.Date)]
        public TimeSpan Birthday { get; set; }

        public Gender Gender { get; set; }

        [DataType(DataType.Currency)]
        public double Salary { get; set; }
    }

    public enum Gender
    {
        Male,
        Female,
        Neutral
    }
}
