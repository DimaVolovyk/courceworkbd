﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkBD.DAL.Models
{
    public class ContactInformation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MinLength(13), MaxLength(13), DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [MinLength(5), DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
