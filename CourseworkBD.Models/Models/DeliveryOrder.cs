﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkBD.DAL.Models
{
    public class DeliveryOrder
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Supplier Supplier { get; set; }

        [DataType(DataType.Date)]
        public TimeSpan OrederDate { get; set; }

        [DataType(DataType.Currency)]
        public double Cost { get; set; }

        public ICollection<Resource> Resources { get; set; }
    }
}
