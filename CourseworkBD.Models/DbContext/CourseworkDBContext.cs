﻿using CourseworkBD.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseworkBD.DAL.DbContext
{
    public class CourseworkDBContext : System.Data.Entity.DbContext
    {
        public CourseworkDBContext() : base("name=CourseworkBD.DAL.Properties.Settings.CourseworkBDConnectionString") { }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Department> Departments { get; set; }
    }
}
