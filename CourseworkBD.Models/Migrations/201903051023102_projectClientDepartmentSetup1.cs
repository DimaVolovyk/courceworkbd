namespace CourseworkBD.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class projectClientDepartmentSetup1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PhoneNumber = c.String(maxLength: 13),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 250),
                        Cost = c.Double(nullable: false),
                        Deadline = c.Time(nullable: false, precision: 7),
                        WorkBegining = c.Time(nullable: false, precision: 7),
                        Address = c.String(maxLength: 500),
                        Client_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id)
                .Index(t => t.Client_Id);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DepartmentProjects",
                c => new
                    {
                        Department_Id = c.Int(nullable: false),
                        Project_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Department_Id, t.Project_Id })
                .ForeignKey("dbo.Departments", t => t.Department_Id, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.Project_Id, cascadeDelete: true)
                .Index(t => t.Department_Id)
                .Index(t => t.Project_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DepartmentProjects", "Project_Id", "dbo.Projects");
            DropForeignKey("dbo.DepartmentProjects", "Department_Id", "dbo.Departments");
            DropForeignKey("dbo.Projects", "Client_Id", "dbo.Clients");
            DropIndex("dbo.DepartmentProjects", new[] { "Project_Id" });
            DropIndex("dbo.DepartmentProjects", new[] { "Department_Id" });
            DropIndex("dbo.Projects", new[] { "Client_Id" });
            DropTable("dbo.DepartmentProjects");
            DropTable("dbo.Departments");
            DropTable("dbo.Projects");
            DropTable("dbo.Clients");
        }
    }
}
