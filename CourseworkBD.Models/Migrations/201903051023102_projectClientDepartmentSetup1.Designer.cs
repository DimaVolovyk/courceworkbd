// <auto-generated />
namespace CourseworkBD.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class projectClientDepartmentSetup1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(projectClientDepartmentSetup1));
        
        string IMigrationMetadata.Id
        {
            get { return "201903051023102_projectClientDepartmentSetup1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
