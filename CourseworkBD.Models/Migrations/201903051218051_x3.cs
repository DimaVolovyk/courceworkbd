namespace CourseworkBD.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class x3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactInformations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(maxLength: 13),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Clients", "ContactInformation_Id", c => c.Int());
            CreateIndex("dbo.Clients", "ContactInformation_Id");
            AddForeignKey("dbo.Clients", "ContactInformation_Id", "dbo.ContactInformations", "Id");
            DropColumn("dbo.Clients", "PhoneNumber");
            DropColumn("dbo.Clients", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "Email", c => c.String());
            AddColumn("dbo.Clients", "PhoneNumber", c => c.String(maxLength: 13));
            DropForeignKey("dbo.Clients", "ContactInformation_Id", "dbo.ContactInformations");
            DropIndex("dbo.Clients", new[] { "ContactInformation_Id" });
            DropColumn("dbo.Clients", "ContactInformation_Id");
            DropTable("dbo.ContactInformations");
        }
    }
}
