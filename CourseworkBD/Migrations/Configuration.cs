namespace CourseworkBD.Migrations
{
    using CourseworkBD.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CourseworkBD.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CourseworkBD.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            
            var role1 = new IdentityRole { Name = "DbAdmin" };
            var role2 = new IdentityRole { Name = "Accountant" };
            var role3 = new IdentityRole { Name = "ProjectManager" };

            if (!roleManager.RoleExists(role1.Name)
                && !roleManager.RoleExists(role2.Name)
                && !roleManager.RoleExists(role3.Name))
            {
                roleManager.Create(role1);
                roleManager.Create(role2);
                roleManager.Create(role3);
            }

            
            var user = new ApplicationUser() { UserName = "An4oyc" };

            if (userManager.FindByName(user.UserName) == null)
            {
                userManager.Create(user, "16481657");
                userManager.AddToRole(user.Id, role1.Name);
            }
        }
    }
}
