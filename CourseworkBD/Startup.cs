﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CourseworkBD.Startup))]
namespace CourseworkBD
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
